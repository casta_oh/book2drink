require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should get users" do
    get :users
    assert_response :success
  end

  test "should get locations" do
    get :locations
    assert_response :success
  end

  test "should get isbninfos" do
    get :isbninfos
    assert_response :success
  end

end
