require 'test_helper'

class CommentbooksControllerTest < ActionController::TestCase
  setup do
    @commentbook = commentbooks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:commentbooks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create commentbook" do
    assert_difference('Commentbook.count') do
      post :create, commentbook: @commentbook.attributes
    end

    assert_redirected_to commentbook_path(assigns(:commentbook))
  end

  test "should show commentbook" do
    get :show, id: @commentbook.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @commentbook.to_param
    assert_response :success
  end

  test "should update commentbook" do
    put :update, id: @commentbook.to_param, commentbook: @commentbook.attributes
    assert_redirected_to commentbook_path(assigns(:commentbook))
  end

  test "should destroy commentbook" do
    assert_difference('Commentbook.count', -1) do
      delete :destroy, id: @commentbook.to_param
    end

    assert_redirected_to commentbooks_path
  end
end
