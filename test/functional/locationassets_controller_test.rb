require 'test_helper'

class LocationassetsControllerTest < ActionController::TestCase
  setup do
    @locationasset = locationassets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:locationassets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create locationasset" do
    assert_difference('Locationasset.count') do
      post :create, locationasset: @locationasset.attributes
    end

    assert_redirected_to locationasset_path(assigns(:locationasset))
  end

  test "should show locationasset" do
    get :show, id: @locationasset.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @locationasset.to_param
    assert_response :success
  end

  test "should update locationasset" do
    put :update, id: @locationasset.to_param, locationasset: @locationasset.attributes
    assert_redirected_to locationasset_path(assigns(:locationasset))
  end

  test "should destroy locationasset" do
    assert_difference('Locationasset.count', -1) do
      delete :destroy, id: @locationasset.to_param
    end

    assert_redirected_to locationassets_path
  end
end
