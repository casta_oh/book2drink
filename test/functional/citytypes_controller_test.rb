require 'test_helper'

class CitytypesControllerTest < ActionController::TestCase
  setup do
    @citytype = citytypes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:citytypes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create citytype" do
    assert_difference('Citytype.count') do
      post :create, citytype: @citytype.attributes
    end

    assert_redirected_to citytype_path(assigns(:citytype))
  end

  test "should show citytype" do
    get :show, id: @citytype.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @citytype.to_param
    assert_response :success
  end

  test "should update citytype" do
    put :update, id: @citytype.to_param, citytype: @citytype.attributes
    assert_redirected_to citytype_path(assigns(:citytype))
  end

  test "should destroy citytype" do
    assert_difference('Citytype.count', -1) do
      delete :destroy, id: @citytype.to_param
    end

    assert_redirected_to citytypes_path
  end
end
