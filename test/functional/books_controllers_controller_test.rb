require 'test_helper'

class BooksControllersControllerTest < ActionController::TestCase
  setup do
    @books_controller = books_controllers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:books_controllers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create books_controller" do
    assert_difference('BooksController.count') do
      post :create, books_controller: @books_controller.attributes
    end

    assert_redirected_to books_controller_path(assigns(:books_controller))
  end

  test "should show books_controller" do
    get :show, id: @books_controller.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @books_controller.to_param
    assert_response :success
  end

  test "should update books_controller" do
    put :update, id: @books_controller.to_param, books_controller: @books_controller.attributes
    assert_redirected_to books_controller_path(assigns(:books_controller))
  end

  test "should destroy books_controller" do
    assert_difference('BooksController.count', -1) do
      delete :destroy, id: @books_controller.to_param
    end

    assert_redirected_to books_controllers_path
  end
end
