require 'test_helper'

class B2DIsbnInfosControllerTest < ActionController::TestCase
  setup do
    @b2_d_isbn_info = b2_d_isbn_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:b2_d_isbn_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create b2_d_isbn_info" do
    assert_difference('B2DIsbnInfo.count') do
      post :create, b2_d_isbn_info: @b2_d_isbn_info.attributes
    end

    assert_redirected_to b2_d_isbn_info_path(assigns(:b2_d_isbn_info))
  end

  test "should show b2_d_isbn_info" do
    get :show, id: @b2_d_isbn_info.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @b2_d_isbn_info.to_param
    assert_response :success
  end

  test "should update b2_d_isbn_info" do
    put :update, id: @b2_d_isbn_info.to_param, b2_d_isbn_info: @b2_d_isbn_info.attributes
    assert_redirected_to b2_d_isbn_info_path(assigns(:b2_d_isbn_info))
  end

  test "should destroy b2_d_isbn_info" do
    assert_difference('B2DIsbnInfo.count', -1) do
      delete :destroy, id: @b2_d_isbn_info.to_param
    end

    assert_redirected_to b2_d_isbn_infos_path
  end
end
