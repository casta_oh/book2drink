require 'test_helper'

class ReadingStatusBooksControllerTest < ActionController::TestCase
  setup do
    @reading_status_book = reading_status_books(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reading_status_books)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reading_status_book" do
    assert_difference('ReadingStatusBook.count') do
      post :create, reading_status_book: @reading_status_book.attributes
    end

    assert_redirected_to reading_status_book_path(assigns(:reading_status_book))
  end

  test "should show reading_status_book" do
    get :show, id: @reading_status_book.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reading_status_book.to_param
    assert_response :success
  end

  test "should update reading_status_book" do
    put :update, id: @reading_status_book.to_param, reading_status_book: @reading_status_book.attributes
    assert_redirected_to reading_status_book_path(assigns(:reading_status_book))
  end

  test "should destroy reading_status_book" do
    assert_difference('ReadingStatusBook.count', -1) do
      delete :destroy, id: @reading_status_book.to_param
    end

    assert_redirected_to reading_status_books_path
  end
end
