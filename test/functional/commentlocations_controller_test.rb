require 'test_helper'

class CommentlocationsControllerTest < ActionController::TestCase
  setup do
    @commentlocation = commentlocations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:commentlocations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create commentlocation" do
    assert_difference('Commentlocation.count') do
      post :create, commentlocation: @commentlocation.attributes
    end

    assert_redirected_to commentlocation_path(assigns(:commentlocation))
  end

  test "should show commentlocation" do
    get :show, id: @commentlocation.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @commentlocation.to_param
    assert_response :success
  end

  test "should update commentlocation" do
    put :update, id: @commentlocation.to_param, commentlocation: @commentlocation.attributes
    assert_redirected_to commentlocation_path(assigns(:commentlocation))
  end

  test "should destroy commentlocation" do
    assert_difference('Commentlocation.count', -1) do
      delete :destroy, id: @commentlocation.to_param
    end

    assert_redirected_to commentlocations_path
  end
end
