require 'test_helper'

class BooksReadedByUsersControllerTest < ActionController::TestCase
  setup do
    @books_readed_by_user = books_readed_by_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:books_readed_by_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create books_readed_by_user" do
    assert_difference('BooksReadedByUser.count') do
      post :create, books_readed_by_user: @books_readed_by_user.attributes
    end

    assert_redirected_to books_readed_by_user_path(assigns(:books_readed_by_user))
  end

  test "should show books_readed_by_user" do
    get :show, id: @books_readed_by_user.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @books_readed_by_user.to_param
    assert_response :success
  end

  test "should update books_readed_by_user" do
    put :update, id: @books_readed_by_user.to_param, books_readed_by_user: @books_readed_by_user.attributes
    assert_redirected_to books_readed_by_user_path(assigns(:books_readed_by_user))
  end

  test "should destroy books_readed_by_user" do
    assert_difference('BooksReadedByUser.count', -1) do
      delete :destroy, id: @books_readed_by_user.to_param
    end

    assert_redirected_to books_readed_by_users_path
  end
end
