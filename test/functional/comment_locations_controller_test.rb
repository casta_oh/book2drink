require 'test_helper'

class CommentLocationsControllerTest < ActionController::TestCase
  setup do
    @comment_location = comment_locations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:comment_locations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create comment_location" do
    assert_difference('CommentLocation.count') do
      post :create, comment_location: @comment_location.attributes
    end

    assert_redirected_to comment_location_path(assigns(:comment_location))
  end

  test "should show comment_location" do
    get :show, id: @comment_location.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @comment_location.to_param
    assert_response :success
  end

  test "should update comment_location" do
    put :update, id: @comment_location.to_param, comment_location: @comment_location.attributes
    assert_redirected_to comment_location_path(assigns(:comment_location))
  end

  test "should destroy comment_location" do
    assert_difference('CommentLocation.count', -1) do
      delete :destroy, id: @comment_location.to_param
    end

    assert_redirected_to comment_locations_path
  end
end
