require 'test_helper'

class IsbninfosControllerTest < ActionController::TestCase
  setup do
    @isbninfo = isbninfos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:isbninfos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create isbninfo" do
    assert_difference('Isbninfo.count') do
      post :create, isbninfo: @isbninfo.attributes
    end

    assert_redirected_to isbninfo_path(assigns(:isbninfo))
  end

  test "should show isbninfo" do
    get :show, id: @isbninfo.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @isbninfo.to_param
    assert_response :success
  end

  test "should update isbninfo" do
    put :update, id: @isbninfo.to_param, isbninfo: @isbninfo.attributes
    assert_redirected_to isbninfo_path(assigns(:isbninfo))
  end

  test "should destroy isbninfo" do
    assert_difference('Isbninfo.count', -1) do
      delete :destroy, id: @isbninfo.to_param
    end

    assert_redirected_to isbninfos_path
  end
end
