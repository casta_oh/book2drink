require 'test_helper'

class UserLibrariesControllerTest < ActionController::TestCase
  setup do
    @user_library = user_libraries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:user_libraries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user_library" do
    assert_difference('UserLibrary.count') do
      post :create, user_library: @user_library.attributes
    end

    assert_redirected_to user_library_path(assigns(:user_library))
  end

  test "should show user_library" do
    get :show, id: @user_library.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user_library.to_param
    assert_response :success
  end

  test "should update user_library" do
    put :update, id: @user_library.to_param, user_library: @user_library.attributes
    assert_redirected_to user_library_path(assigns(:user_library))
  end

  test "should destroy user_library" do
    assert_difference('UserLibrary.count', -1) do
      delete :destroy, id: @user_library.to_param
    end

    assert_redirected_to user_libraries_path
  end
end
