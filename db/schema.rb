# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130203102355) do

  create_table "activities", :force => true do |t|
    t.integer  "reference",        :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "activity_type_id"
    t.integer  "user_id"
  end

  add_index "activities", ["activity_type_id"], :name => "index_activities_on_activity_type_id"
  add_index "activities", ["user_id"], :name => "index_activities_on_user_id"

  create_table "activity_types", :force => true do |t|
    t.string   "activity_type", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assets", :force => true do |t|
    t.string   "asset_file_name"
    t.string   "asset_content_type"
    t.integer  "asset_file_size"
    t.datetime "asset_updated_at"
    t.integer  "location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "assets", ["location_id"], :name => "index_assets_on_location_id"

  create_table "book_types", :force => true do |t|
    t.string   "type",       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "books", :force => true do |t|
    t.datetime "donation_date"
    t.integer  "location_id"
    t.integer  "isbninfo_id"
    t.integer  "donation_user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "books", ["donation_user_id"], :name => "index_books_on_user_id"
  add_index "books", ["isbninfo_id"], :name => "index_books_on_isbninfo_id"
  add_index "books", ["location_id"], :name => "index_books_on_location_id"

  create_table "books_readed_by_users", :force => true do |t|
    t.integer  "user_id",     :null => false
    t.integer  "isbninfo_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "books_readed_by_users", ["isbninfo_id"], :name => "index_books_readed_by_users_on_isbninfo_id"
  add_index "books_readed_by_users", ["user_id"], :name => "index_books_readed_by_users_on_user_id"

  create_table "city_types", :force => true do |t|
    t.string   "name",             :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "country_types_id"
  end

  add_index "city_types", ["country_types_id"], :name => "index_city_types_on_country_types_id"

  create_table "citytypes", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "commentbooks", :force => true do |t|
    t.datetime "commentdate"
    t.string   "commenttext"
    t.integer  "isbninfo_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "commentbooks", ["isbninfo_id"], :name => "index_commentbooks_on_isbninfo_id"
  add_index "commentbooks", ["user_id"], :name => "index_commentbooks_on_user_id"

  create_table "commentlocations", :force => true do |t|
    t.string   "commenttext", :null => false
    t.integer  "user_id"
    t.integer  "location_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "commentlocations", ["location_id"], :name => "index_commentlocations_on_location_id"
  add_index "commentlocations", ["user_id"], :name => "index_commentlocations_on_user_id"

  create_table "country_types", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "documents", :force => true do |t|
    t.integer  "group_id",   :null => false
    t.integer  "group_id2",  :null => false
    t.datetime "date_added", :null => false
    t.string   "title",      :null => false
    t.text     "content",    :null => false
  end

  create_table "events", :force => true do |t|
    t.string   "name",                              :null => false
    t.string   "description",                       :null => false
    t.datetime "event_date"
    t.string   "image"
    t.integer  "active",             :default => 1, :null => false
    t.integer  "location_id",                       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
  end

  add_index "events", ["location_id"], :name => "index_events_on_location_id"

  create_table "find_googles", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "isbninfos", :force => true do |t|
    t.string   "isbn10",                                              :null => false
    t.string   "title",                                               :null => false
    t.string   "author",                                              :null => false
    t.boolean  "unique",                            :default => true, :null => false
    t.string   "editorial"
    t.integer  "score",                             :default => 1000, :null => false
    t.integer  "publish_year"
    t.integer  "pages"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.string   "description",        :limit => 800
    t.string   "isbn13"
    t.integer  "user_id"
  end

  add_index "isbninfos", ["user_id"], :name => "index_isbninfos_on_user_id"

  create_table "locations", :force => true do |t|
    t.string   "name",                                 :null => false
    t.string   "contact_name",                         :null => false
    t.string   "contact_phone",                        :null => false
    t.string   "address",                              :null => false
    t.string   "contact_email",                        :null => false
    t.string   "local_phone",                          :null => false
    t.string   "metro_info"
    t.string   "bus_info"
    t.integer  "score",              :default => 1000, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "schedule"
    t.integer  "capacity"
    t.string   "specialty"
    t.string   "description",        :default => "",   :null => false
    t.integer  "city_type_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "gmaps"
    t.string   "web"
  end

  add_index "locations", ["city_type_id"], :name => "index_locations_on_city_type_id"

  create_table "promotions", :force => true do |t|
    t.string   "name",                              :null => false
    t.string   "description",                       :null => false
    t.string   "image"
    t.integer  "active",             :default => 1, :null => false
    t.datetime "begin_date"
    t.datetime "end_date"
    t.integer  "location_id",                       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
  end

  add_index "promotions", ["location_id"], :name => "index_promotions_on_location_id"

  create_table "reading_status_books", :force => true do |t|
    t.string   "reading_type", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "search_books", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tags", :id => false, :force => true do |t|
    t.integer "docid", :null => false
    t.integer "tagid", :null => false
  end

  add_index "tags", ["docid", "tagid"], :name => "docid", :unique => true

  create_table "user_libraries", :force => true do |t|
    t.boolean  "favorite",               :default => false, :null => false
    t.boolean  "wished",                 :default => false, :null => false
    t.integer  "isbninfo_id",                               :null => false
    t.integer  "user_id",                                   :null => false
    t.integer  "reading_status_book_id",                    :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "book_types_id"
  end

  add_index "user_libraries", ["book_types_id"], :name => "index_user_libraries_on_book_types_id"
  add_index "user_libraries", ["isbninfo_id"], :name => "index_user_libraries_on_isbninfo_id"
  add_index "user_libraries", ["reading_status_book_id"], :name => "index_user_libraries_on_reading_status_book_id"
  add_index "user_libraries", ["user_id"], :name => "index_user_libraries_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "name"
    t.string   "surname"
    t.string   "email",                  :default => "",           :null => false
    t.string   "telele"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "city_type_id"
    t.integer  "locations_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.string   "encrypted_password",     :default => "",           :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "provider"
    t.string   "uid"
    t.boolean  "admin",                  :default => false
    t.date     "data_term",              :default => '2013-01-07', :null => false
    t.boolean  "term",                   :default => false,        :null => false
  end

  add_index "users", ["city_type_id"], :name => "index_users_on_city_type_id"
  add_index "users", ["locations_id"], :name => "index_users_on_locations_id"

end
