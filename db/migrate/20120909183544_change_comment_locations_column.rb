class ChangeCommentLocationsColumn < ActiveRecord::Migration
  def up

    change_column :commentlocations, :user_id, :integer, :null => true
  end

  def down
  end
end
