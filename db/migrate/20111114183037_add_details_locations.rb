class AddDetailsLocations < ActiveRecord::Migration
  def change
    
    add_column :locations, :specialty, :string
    add_column :locations, :description, :string, :null => false, :default => ""
    rename_column :locations, :owner_email, :contact_email
    
  end
  
end
