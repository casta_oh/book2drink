class CreateUserLibraries < ActiveRecord::Migration
  def change
    create_table :user_libraries do |t|
      t.boolean :favorite, :default => false, :null => false
      t.boolean :wished, :default => false, :null => false
      t.references :isbninfo, :null => false
      t.references :user, :null => false
      t.references :reading_status_book, :null => false
      t.timestamps
    end
    
    add_index :user_libraries, :isbninfo_id
    add_index :user_libraries, :user_id
    add_index :user_libraries, :reading_status_book_id
  end
end
