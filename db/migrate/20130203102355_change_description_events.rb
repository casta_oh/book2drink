class ChangeDescriptionEvents < ActiveRecord::Migration
  def up
    change_column :events, :description, :string, :size => 5000
  end

  def down
  end
end
