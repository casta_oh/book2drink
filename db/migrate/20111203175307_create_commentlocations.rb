class CreateCommentlocations < ActiveRecord::Migration
  def change
    create_table :commentlocations do |t|
      t.string :commenttext, :null => false, :length => 1024
      t.references :user, :null => false
      t.references :location, :null => false
      t.timestamps
    end
    
    add_index :commentlocations, :location_id
    add_index :commentlocations, :user_id
    
  end
end
