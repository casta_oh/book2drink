class AddDetailsToLocations < ActiveRecord::Migration
    
    def self.up
    
      change_table :locations do |t|
      
        t.references :city_type
        add_index :locations, :city_type_id
      end
      
    end
    
    
end
