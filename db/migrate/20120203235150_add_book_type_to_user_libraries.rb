class AddBookTypeToUserLibraries < ActiveRecord::Migration
  def change
    change_table :user_libraries do |t|
          t.references :book_types
          add_index :user_libraries, :book_types_id
       end
    
  end
end
