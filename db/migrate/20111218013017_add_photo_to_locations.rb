class AddPhotoToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :photo_file_name, :string
    add_column :locations, :photo_content_type, :string
    add_column :locations, :photo_file_size, :integer
  end
end
