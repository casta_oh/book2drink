class CreateBooksReadedByUsers < ActiveRecord::Migration
  def change
    create_table :books_readed_by_users do |t|
      
      t.references :user, :null => false
      t.references :isbninfo, :null => false
      t.timestamps
    end

    add_index :books_readed_by_users, :user_id
    add_index :books_readed_by_users, :isbninfo_id
    
  end
end
