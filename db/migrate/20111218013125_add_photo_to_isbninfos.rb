class AddPhotoToIsbninfos < ActiveRecord::Migration
  def change
    add_column :isbninfos, :photo_file_name, :string
    add_column :isbninfos, :photo_content_type, :string
    add_column :isbninfos, :photo_file_size, :integer
  end
end
