class AddTermsToUsers < ActiveRecord::Migration
  def change

    add_column :users, :term, :boolean, :null => false, :default => false

  end
end
