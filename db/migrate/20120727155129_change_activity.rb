class ChangeActivity < ActiveRecord::Migration

  def change

    #remove_column :activities, :type_id
    #remove_column :activities, :user_id
    #remove_column :activities, :activity_types_id

    change_table :activities do |t|
      t.references :activity_type
      t.references :user

    end
    add_index :activities, :activity_type_id
    add_index :activities, :user_id

  end


end
