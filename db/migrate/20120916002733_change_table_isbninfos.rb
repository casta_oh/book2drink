class ChangeTableIsbninfos < ActiveRecord::Migration
  def up
    
    rename_column :isbninfos, :isbn, :isbn10
    add_column :isbninfos, :isbn13, :string, :size => 13, :null => true
    
  end

  def down
  end
end
