class CreateActivityTypes < ActiveRecord::Migration
  def change
    create_table :activity_types do |t|
      t.string :type, :null => false

      t.timestamps
    end
  end
end
