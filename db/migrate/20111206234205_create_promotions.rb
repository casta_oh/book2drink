class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|

      t.string :name, :null => false, :length => 1024
      t.string :description, :null => false, :length => 2048
      t.string :image, :length => 512
      t.integer :active, :null => false, :default => 1, :length => 1
      t.datetime :begin_date
      t.datetime :end_date
      t.references :location, :null => false
      
      t.timestamps
    end
    
    add_index :promotions, :location_id
    
  end
end
