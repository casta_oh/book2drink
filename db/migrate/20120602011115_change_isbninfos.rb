class ChangeIsbninfos < ActiveRecord::Migration
 def change
   add_column :isbninfos, :description, :string, :length => 1000
   change_column :isbninfos, :editorial, :string, :null => true

 end
end
