class CreateCountryTypes < ActiveRecord::Migration
  def self.up
    create_table :country_types, :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8' do |t|
      t.string :name, :null => false, :unique => true, :size => 1000
      t.timestamps
    end

    #Populate table países
    CountryTypes.create :name => 'Espa&ntilde;a'


  end
end
