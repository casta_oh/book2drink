class ChangeTableActivity < ActiveRecord::Migration

  def change

    remove_column :activities, :type_id
    remove_column :activities, :user_id
    #remove_column :activities, :activity_types_id

  end

  
  #def change
  #
  #  change_table :activities do |t|
  #    t.remove_column :type_id
  #    t.remove_column :user_id
  #    t.remove_column :activity_types_id
  #  end
  #end

end
