class ChangeUserBooks < ActiveRecord::Migration
  
  def change
            rename_column :books, :user_id, :donation_user_id
  end
end
