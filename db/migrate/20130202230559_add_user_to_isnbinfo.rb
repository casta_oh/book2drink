class AddUserToIsnbinfo < ActiveRecord::Migration
  def change
    change_table :isbninfos do |t|
          t.references :user
          add_index :isbninfos, :user_id
    end

  end
end
