class CreateIsbninfos < ActiveRecord::Migration
  def change
    create_table :isbninfos do |t|

      t.string :isbn , :null => false
      t.string :title, :null => false
      t.string :author, :null => false
      t.boolean :unique, :null => false, :default => true
      t.string :editorial, :null => false
      t.integer :score, :null => false, :default => 1000
      t.integer :publish_year
      t.integer :pages
      
      t.timestamps
    end
  end
end
