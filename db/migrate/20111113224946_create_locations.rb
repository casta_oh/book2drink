class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name, :null => false
      t.string :contact_name, :null => false
      t.string :contact_phone, :null => false
      t.string :address, :null => false
      t.integer :address_x
      t.integer :address_y
      t.string :owner_email, :null => false
      t.string :local_phone, :null => false
      t.string :owner_email, :null => false
      t.string :metro_info
      t.string :bus_info
      t.integer :score, :null => false, :default => 1000
      
      t.timestamps
    end
  end
end
