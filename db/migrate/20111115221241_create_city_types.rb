class CreateCityTypes < ActiveRecord::Migration
  def self.up

    create_table :city_types, :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8' do |t|
      t.string :name, :null => false
      t.integer :province_id, :null => false

      t.timestamps
    end
  end
end
