class AddCityToUsers < ActiveRecord::Migration
  
  def self.up
    
      change_table :users do |t|
      
        t.references :city_type
        add_index :users, :city_type_id
      end
      
    end
    
  
  
end
