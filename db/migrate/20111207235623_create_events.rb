class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|

      t.string :name, :null => false, :length => 1024
      t.string :description, :null => false, :length => 2048
      t.datetime :event_date
      t.string :image, :length => 512
      t.integer :active, :null => false, :default => 1, :length => 1
      t.references :location, :null => false
      t.timestamps
    end
    add_index :events, :location_id
  end
end
