class CreateBooks < ActiveRecord::Migration
  
  def up
      create_table :books do |t|
        
        t.datetime :donation_date
        #t.integer :donation_user_id, :references => :users
        t.references :location
        t.references :isbninfo
        t.references :user
        
        t.timestamps
      end
    add_index :books, :location_id
    add_index :books, :isbninfo_id
    add_index :books, :user_id
           
    end

  def down
    drop_table :books
  end

end
