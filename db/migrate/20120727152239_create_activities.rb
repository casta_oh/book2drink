class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :type_id, :null => false
      t.integer :reference, :null => false
      t.integer :user_id, :null => false
      t.timestamps
    end
  end
end
