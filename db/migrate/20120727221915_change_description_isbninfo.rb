class ChangeDescriptionIsbninfo < ActiveRecord::Migration
  def up
    change_column :isbninfos, :description, :string, :size => 800

  end

  def down
  end
end
