class CreateBookTypes < ActiveRecord::Migration
  def change
    create_table :book_types do |t|
      t.string :description, :null => false, :length => 100
      t.timestamps
    end
  end
end
