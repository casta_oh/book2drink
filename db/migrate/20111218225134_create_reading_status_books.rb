class CreateReadingStatusBooks < ActiveRecord::Migration
  def change
    create_table :reading_status_books do |t|
      t.string :reading_type, :null => false, :length => 1024
      t.timestamps
    end

    ReadingStatusBooks.create :reading_type => 'Por leer'
    ReadingStatusBooks.create :reading_type => 'Leyendo'
    ReadingStatusBooks.create :reading_type => 'Leído'

  end
end
