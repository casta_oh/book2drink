class AddPhotoToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :photo_file_name, :string
    add_column :promotions, :photo_content_type, :string
    add_column :promotions, :photo_file_size, :integer
  end
end
