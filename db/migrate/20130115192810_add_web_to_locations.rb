class AddWebToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :web, :string, :null => true, :size => 300
  end
end
