class AddLocationToUsers < ActiveRecord::Migration
  def self.up
      
        change_table :users do |t|
        
          t.references :locations
          add_index :users, :locations_id
        end
        
      end
end
