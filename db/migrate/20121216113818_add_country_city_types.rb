class AddCountryCityTypes < ActiveRecord::Migration
  
  def change
    change_table :city_types do |t|
          t.references :country_types
          add_index :city_types, :country_types_id
    end

  end
end
