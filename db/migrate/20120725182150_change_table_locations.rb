class ChangeTableLocations < ActiveRecord::Migration
 
  def up

    remove_column :locations, :address_x
    remove_column :locations, :address_y

  end

  def down
  end
end
