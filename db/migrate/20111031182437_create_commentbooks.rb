class CreateCommentbooks < ActiveRecord::Migration
  def change
    create_table :commentbooks do |t|
      t.datetime :commentdate
      t.string :commenttext
      t.references :isbninfo
      t.references :user

      t.timestamps
    end
    add_index :commentbooks, :isbninfo_id
    add_index :commentbooks, :user_id
  end
end
