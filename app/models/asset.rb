class Asset < ActiveRecord::Base

  belongs_to :post
  has_attached_file :asset,
    :styles => {
      :thumb=> "100x100#",
      :small  => "400x400>", 
      :large => "640X480" },
    :storage => :s3,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => "/:style/:id/:filename",
    :url  => ":s3_eu_url"
  validates_attachment_content_type :asset, :content_type => ['image/gif', 'image/jpeg', 'image/png', 'image/x-ms-bmp']

end
