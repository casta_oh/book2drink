# encoding: utf-8
require 'open-uri'
class Isbninfo < ActiveRecord::Base

  has_many :commentbooks
  has_many :books
  has_many :books_readed_by_users
  has_many :user_libraries
  belongs_to :user
 
  def self.search_indice(cadena, page, per_page)
    @isbninfos = Isbninfo.search cadena, :page => page, :per_page => per_page
  end
  
  def self.self_search_old_style(cadena, page)
    cadena = "%" + cadena + "%"
    @isbninfos = Isbninfo.where("title like :search_string", {:search_string => cadena})
    @isbninfos = @isbninfos.paginate(:page => page, :per_page => Book2Drink::Application.config.resultsperpage)
  end

  has_attached_file :photo,
        :styles => {
          :thumb=> "100x100#",
          :small  => "400x400>",
          :large => "640X480" },
     :storage => :s3,
     :s3_credentials => "#{Rails.root}/config/s3.yml",
     :path => "/:style/:id/:filename",
     :url  => ":s3_eu_url"
  validates_attachment_content_type :photo, :content_type => ['image/gif', 'image/jpeg', 'image/png', 'image/x-ms-bmp']


  #Get the picture from a given url.
  def picture_from_url(url)
      self.photo = open(url)
  end

  def self.parse_google_book(params, user_id)

    #<%= hidden_field_tag "isbn13book", libro.isbn_13 %>
    #<%= hidden_field_tag "isbn10book", libro.isbn_10 %>
    #<%= hidden_field_tag "title", libro.title %>
    #<%= hidden_field_tag "authors", libro.authors %>
    #<%= hidden_field_tag "editorial", libro.editorial %>
    #<%= hidden_field_tag "publish_year", libro.publish_year %>
    #<%= hidden_field_tag "pages", libro.pages %>
    #<%= hidden_field_tag "description", libro.description %>
    #<%= hidden_field_tag "image_link", libro.image_link %>

    @isbninfo = User.find(user_id).isbninfos.build
    #@isbninfo = Isbninfo.new
    @isbninfo.isbn13 = params[:isbn13book]
    @isbninfo.isbn10 = params[:isbn10book]
    @isbninfo.title = params[:title]
    @isbninfo.author = params[:authors]
    @isbninfo.editorial = params[:editorial]
    @isbninfo.publish_year = params[:published_date]
    @isbninfo.pages = params[:page_count]
    
    if (params[:description] != nil and params[:description] != '')
      @isbninfo.description = params[:description][0..799]
    else
      @isbninfo.description = 'No hay ninguna descripción del libro.'
    end
    if (params[:image_link] != nil and params[:image_link] != '')
      @isbninfo.picture_from_url(params[:image_link])
    end
    return @isbninfo
  end

end
