require 'open-uri'
require 'nokogiri'

class FindGoogle
  
  attr_accessor :title, :description
   
  def self.from_google(title)
      book  = self.new
      entry = Nokogiri::XML(open "http://books.google.com/books/feeds/volumes?q=#{title}").css("entry id").first
      xml   = Nokogiri::XML(open entry.text) if entry
      return book unless xml
  
      book.title       = xml.css("entry dc|title").first.text       unless xml.css("entry dc|title").empty?
      book.description = xml.css("entry dc|description").first.text unless xml.css("entry dc|description").empty?
      book
    end
  
end
