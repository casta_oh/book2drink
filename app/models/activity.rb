class Activity < ActiveRecord::Base
  belongs_to :user
  belongs_to :activity_type

  def self.create_activity (user_id, type_id, reference)
    @activity = Activity.new
    @activity.reference = reference
    @activity.user_id = user_id
    @activity.activity_type_id = type_id
    @activity.save
  end

end
