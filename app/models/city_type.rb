class CityType < ActiveRecord::Base
  has_many :locations
  has_many :users
  belongs_to :country_types
end
