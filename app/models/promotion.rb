class Promotion < ActiveRecord::Base
  belongs_to :location
  #hay que poner los campos fecha inicio y fecha final como obligatorios en la base de datos
  
  has_attached_file :photo,
        :styles => {
          :thumb=> "100x100#",
          :small  => "400x400>",
          :large => "640X480" },
     :s3_credentials => "#{Rails.root}/config/s3.yml",
     :path => "/:style/:id/:filename",
     :url  => ":s3_eu_url"
   validates_attachment_content_type :photo, :content_type => ['image/gif', 'image/jpeg', 'image/png', 'image/x-ms-bmp']
          
end
