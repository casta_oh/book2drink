# encoding: utf-8
require 'open-uri'
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  # Setup accessible (or protected) attributes for your model
   attr_accessible :email, :password, :password_confirmation, :remember_me, :provider, :uid, :name, :city_type_id, :locations_id, :photo, :username, :term
  #acts_as_authentic

  has_many :commentbooks, :dependent => :nullify
  has_many :commentlocations, :dependent => :nullify
  belongs_to :city_types
  belongs_to :location
  has_many :books_readed_by_users, :dependent => :delete_all
  has_many :user_libraries, :dependent => :delete_all
  has_many :activities, :dependent => :delete_all
  has_many :isbninfos, :dependent => :nullify

  has_attached_file :photo,
    :styles => {
      :thumb=> "100x100#",
      :small  => "400x400>",
      :large => "640X480" },
    :storage => :s3,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => "/:style/:id/:filename",
    :url  => ":s3_eu_url"
  validates_attachment_content_type :photo, :content_type => ['image/gif', 'image/jpeg', 'image/png', 'image/x-ms-bmp']
  validates :username, :presence => true, :uniqueness => true, :length => { :maximum => 16 }
  validates :name, :presence => true, :length => { :maximum => 32 }
  validates_acceptance_of :term, :accept => true, :allow_nil => false, :message => "Debes aceptar los términos y condiciones de uso", :on => :create

   #Get the picture from a given url.
  def picture_from_url(url)
      self.photo = open(url)
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user

      user = User.create(
        provider:auth.provider,
        uid:auth.uid,
        email:auth.info.email,
        password:Devise.friendly_token[0,20],
        term: true,
        username: auth.info.nickname
        )
      user.picture_from_url(auth.info.image)
      user.name = auth.info.first_name
      user.surname = auth.info.last_name
      
    end

    puts "El nuevo usuario" + user.to_s
    puts "El nuevo usuario" + user.username.to_s

    user
  end

  def self.create_user(registration_hash)
    logger.info "Creating new user with registration hash: #{registration_hash.to_yaml}"
    unless registration_hash or resigration_hash.empty?
      return nil
    end
    user = User.new
    user.email = registration_hash[:email]
    if registration_hash[:password]
      user.password = registration_hash[:password]
    else
      user.password = Devise.friendly_token[0,20]
    end
    user.password_confirmation = user.password

    # custom app code here...


    if registration_hash[:skip_confirmation] == true
      user.confirm!
    end

    user
  end
  
end
