class Location < ActiveRecord::Base
  acts_as_gmappable :process_geocoding => false

  #Configuración de google maps
  #https://github.com/apneadiving/Google-Maps-for-Rails/wiki/Markers
  def gmaps4rails_address
    #"#{self.name}, #{self.contact_name}, #{self.contact_phone}"
    "#{self.address}"
  end

  def gmaps4rails_infowindow
      str = "<div class=\"gmLocation\">"
      str = str + "<img src=\"#{self.photo.url(:small)}\" class=\"thumbPhotoCarouselLocation\" style=\"float: left;\">"
      str = str +
        "<div style=\"float: left; margin: 5px;\">
            <div class=\"etiqueta\">"
      str = str + "Nombre: " + "<a href=\"/locations/" + self.id.to_s + " \">" + self.name + "</a>"
      str = str +
            "</div>
            <div class=\"etiqueta\">"
      str = str + "Direcci&oacute;n: " + self.address
      str = str +
            "</div>
            <div class=\"etiqueta\">
              Tel&eacute;fono: " + self.local_phone +
            "</div>"
      str = str + "</div>"

      puts str
      return str
  end

  def gmaps4rails_title
      self.name
  end
  belongs_to :city_types
  has_many :users
  has_many :commentlocations
  has_many :promotions
  has_many :events
  has_many :assets
  
  attr_accessible  :name, :description, :local_phone, :schedule, :capacity, :specialty, :contact_name, :contact_phone,
            :contact_email, :address, :latitude, :longitude, :bus_info, :metro_info, :city_type_id,
            :assets_attributes, :photo, :web
  accepts_nested_attributes_for :assets, :allow_destroy => true

  has_attached_file :photo,
        :styles => {
          :thumb=> "100x100#",
          :small  => "400x400>",
          :large => "640X480" },
     :storage => :s3,
     :s3_credentials => "#{Rails.root}/config/s3.yml",
     :path => "/:style/:id/:filename",
     :url  => ":s3_eu_url"
   validates_attachment_content_type :photo, :content_type => ['image/gif', 'image/jpeg', 'image/png', 'image/x-ms-bmp']

  
          
end

