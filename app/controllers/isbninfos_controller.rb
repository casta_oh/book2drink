class IsbninfosController < ApplicationController
  include IsbninfosHelper
  include UserLibrariesHelper
  protect_from_forgery
  before_filter :verify_admin, :only => [:edit, :update, :destroy, :new]
  # GET /isbninfos
  # GET /isbninfos.json
  def index
    
    @search = params[:search]
    #método con índices. CHACHI
    #@isbninfos = Isbninfo.search_indice(@search, params[:page], 15)
    
    #método cutre. CACA
    #@isbninfos = Isbninfo.self_search_old_style(@search, params[:page])
    if (@search.nil? or @search == "")
      @isbninfos = Isbninfo.find(:all, :order => 'created_at DESC')
      @totalFoundIsbnInfos = @isbninfos.size
      @isbninfos = Kaminari.paginate_array(@isbninfos).page(params[:page])
                            .per(Book2Drink::Application.config.resultsperpage)


    else
      escaped_search = params[:search].gsub('%','\%').gsub('_','\_')
      escaped_search = "%" + escaped_search + "%"
      escaped_search = escaped_search.downcase
      @isbninfos = Isbninfo
      .where("lower(title) like ? OR lower(author) like ? OR lower(editorial) like ?", escaped_search, escaped_search, escaped_search)
      @totalFoundIsbnInfos = @isbninfos.length
      @isbninfos = @isbninfos.page(params[:page])
        .per(Book2Drink::Application.config.resultsperpage)
    end

    #cadena con will_paginate @isbninfos = @isbninfos.paginate(:page => params[:page], :per_page => Book2Drink::Application.config.resultsperpage)
    
    #page es así params[:page].to_i
    if @search != nil
      puts "voy a buscar por el termino" + @search.to_s
    end

    #Si con esta línea no se devuelven libros, probar con la otra.
    #@libros = GoogleBooks.search(@search, {:count => 40, :page => 1, :country => 'EU'})
    #Esta línea es porque a veces google no devuelve libros, entonces meto la ip del hosting de aruba
    @libros = GoogleBooks.search(@search, {:count => 40, :page => 1}, '87.219.38.12')
    puts "El numero de libros recuperado es: " + @libros.count.to_s
    #@googlebooks = @googlebooks.paginate(:page => params[:page].to_i, :per_page => 15)
    
    if current_user != nil
      @user_libraries = user_library
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @isbninfos }
    end
  end

  def indexsearchbooks
    puts "estoy en indexsearchbooks"
    @isbninfos = Isbninfo.paginate(:page => params[:page], :per_page => Book2Drink::Application.config.resultsperpage)
    @search = params[:search]
    if @search != nil and @search != ''
      puts "estoy en index " + @search
    end

    #@libros = GoogleBooks.search(@search, {:count => Book2Drink::Application.config.resultsperpage, :country => 'EU'})
    #Esta línea es porque a veces google no devuelve libros, entonces meto la ip del hosting de aruba
    @libros = GoogleBooks.search(@search, {:count => Book2Drink::Application.config.resultsperpage, :page => 1}, '87.219.38.12')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @isbninfos }
    end

  end
 
  
  # GET /isbninfos/1
  # GET /isbninfos/1.json
  def show
    @isbninfo = Isbninfo.find(params[:id])
    @commentbooks = @isbninfo.commentbooks
    
    #Se busca si el libro ya está añadido a la librería del usuario
    if (!current_user.nil?)
      @user_libraries = current_user.user_libraries
      @user_library = @user_libraries.where(:isbninfo_id => @isbninfo.id).first
    end
    #@user = User.where("username = ?", "pepa").first
  end

  # GET /isbninfos/new
  # GET /isbninfos/new.json
  def new
    @isbninfo = Isbninfo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @isbninfo }
    end
  end

  # GET /isbninfos/1/edit
  def edit
    @isbninfo = Isbninfo.find(params[:id])
  end

  # POST /isbninfos
  # POST /isbninfos.json
  def create
    @isbninfo = Isbninfo.new(params[:isbninfo])

    respond_to do |format|
      if @isbninfo.save
        format.html { redirect_to @isbninfo, notice: 'Se ha creado un nuevo libro.' }
        format.json { render json: @isbninfo, status: :created, location: @isbninfo }
      else
        format.html { render action: "new" }
        format.json { render json: @isbninfo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /isbninfos/1
  # PUT /isbninfos/1.json
  def update
    @isbninfo = Isbninfo.find(params[:id])

    respond_to do |format|
      if @isbninfo.update_attributes(params[:isbninfo])
        format.html { redirect_to @isbninfo, notice: 'Isbninfo was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @isbninfo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /isbninfos/1
  # DELETE /isbninfos/1.json
  def destroy
    @isbninfo = Isbninfo.find(params[:id])
    @isbninfo.destroy

    respond_to do |format|
      format.html { redirect_to isbninfos_url }
      format.json { head :ok }
    end
  end
  
  #POST
  def addtobooks

    #isbninfo13 = params[:isbn13book]
    #isbninfo10 = params[:isbn10book]

    @isbninfo = Isbninfo.parse_google_book(params, current_user.id)

    #puts "Voy a busca por isbn13: " + isbninfo13.to_s
    #puts "Voy a busca por isbn10: " + isbninfo10.to_s
    #if (isbninfo13.to_s == "")
    #  search = isbninfo10
    #else
    #  search = isbninfo13
    #end
    #puts "Voy a buscar por el libro: " + search
    #@libros = GoogleBooks.search(search, {:count => 1}, '62.149.128.151')
    #puts "He encontrado libros en google"
    #@isbninfo = Isbninfo.parse_google_book(@libros.first, current_user.id)

    puts "He parseado el libro de google a b2d"
    @isbninfo = save_isbninfo_check_repetition(@isbninfo)
    puts "he guardado el libro en b2d"
    if (@isbninfo != nil)
      #libro guardado. Se asigna al usuario a la librería.
      @user_library = @isbninfo.user_libraries.new
      @user_library.user_id = current_user.id
      @user_library.reading_status_book_id = 1

      respond_to do |format|
        @user_library = save_userlibrary_check_repetition(@user_library)
        if @user_library != nil
          format.html { redirect_to :controller => "user_libraries", :action => "index"}
        else
          format.html { render action: "index" }
          format.json { render json: @user_library.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        format.html { render action: "index" }
        format.json { render json: @isbninfo.errors, status: :unprocessable_entity }
      end
    end

  end

end
