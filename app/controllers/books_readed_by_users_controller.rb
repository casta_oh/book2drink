class BooksReadedByUsersController < ApplicationController
  protect_from_forgery
  before_filter :verify_admin

  # GET /books_readed_by_users
  # GET /books_readed_by_users.json
  def index
    @user = current_user
    @books_readed_by_users = @user.books_readed_by_users

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @books_readed_by_users }
    end
  end

  # GET /books_readed_by_users/1
  # GET /books_readed_by_users/1.json
  def show
    @books_readed_by_user = BooksReadedByUser.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @books_readed_by_user }
    end
  end

  # GET /books_readed_by_users/new
  # GET /books_readed_by_users/new.json
  def new
    @books_readed_by_user = BooksReadedByUser.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @books_readed_by_user }
    end
  end

  # GET /books_readed_by_users/1/edit
  def edit
    @books_readed_by_user = BooksReadedByUser.find(params[:id])
  end

  # POST /books_readed_by_users
  # POST /books_readed_by_users.json
  def create
    @books_readed_by_user = BooksReadedByUser.new(params[:books_readed_by_user])

    respond_to do |format|
      if @books_readed_by_user.save
        format.html { redirect_to @books_readed_by_user, notice: 'Books readed by user was successfully created.' }
        format.json { render json: @books_readed_by_user, status: :created, location: @books_readed_by_user }
      else
        format.html { render action: "new" }
        format.json { render json: @books_readed_by_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /books_readed_by_users/1
  # PUT /books_readed_by_users/1.json
  def update
    @books_readed_by_user = BooksReadedByUser.find(params[:id])

    respond_to do |format|
      if @books_readed_by_user.update_attributes(params[:books_readed_by_user])
        format.html { redirect_to @books_readed_by_user, notice: 'Books readed by user was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @books_readed_by_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books_readed_by_users/1
  # DELETE /books_readed_by_users/1.json
  def destroy
    @books_readed_by_user = BooksReadedByUser.find(params[:id])
    @books_readed_by_user.destroy

    respond_to do |format|
      format.html { redirect_to books_readed_by_users_url }
      format.json { head :ok }
    end
  end
end
