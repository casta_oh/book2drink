class UserLibrariesController < ApplicationController
  include UserLibrariesHelper
  protect_from_forgery
  before_filter :verify_admin, :only => [:show, :new, :edit]

  # GET /user_libraries
  # GET /user_libraries.json
  def index
    @mode = params[:mode]
    if @mode == 'favorites' 
      @user_libraries = user_library.where(:favorite => true)
    elsif @mode == 'wished'
      @user_libraries = user_library.where(:wished => true)
    else 
      @user_libraries = user_library.all
    end

    @user_libraries = Kaminari.paginate_array(@user_libraries)
              .page(params[:page])
              .per(Book2Drink::Application.config.resultsperpage)

    @totalUserLibrary = current_user.user_libraries.length
    #user_library_by_id(@user.id).length

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @user_libraries }
    end
  end

  #este método obtiene la librería del usuario pasado por parámetro
  def public_userlibrary
    @mode = params[:mode]
    @user = User.find(params[:id])
    @totalUserLibrary = user_library_by_id(@user.id).length
    if @mode == 'favorites'
      @user_libraries = user_library_by_id(@user.id).where(:favorite => true)
    elsif @mode == 'wished'
      @user_libraries = user_library_by_id(@user.id).where(:wished => true)
    else
      @user_libraries = user_library_by_id(@user.id).all
    end

    @user_libraries = Kaminari.paginate_array(@user_libraries)
              .page(params[:page])
              .per(Book2Drink::Application.config.resultsperpage)

  end

  # GET /user_libraries/1
  # GET /user_libraries/1.json
  def show
    @user_library = UserLibrary.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user_library }
    end
  end

  # GET /user_libraries/new
  # GET /user_libraries/new.json
  def new
    @user_library = UserLibrary.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_library }
    end
  end

  # GET /user_libraries/1/edit
  def edit
    @user_library = UserLibrary.find(params[:id])
  end

  # POST /user_libraries
  # POST /user_libraries.json
  def create
    @user_library = UserLibrary.new(params[:user_library])
     #se pone por defecto el estado de libro: "por leer"
    @user_library.isbninfo_id = params[:user_library][:isbninfo_id]
    @user_library.user_id = params[:user_library][:user_id]
    @user_library.reading_status_book_id = 1
    
    respond_to do |format|

      @user_library = save_userlibrary_check_repetition(@user_library)
      if (@user_library != nil)
        format.html { redirect_to :controller => "user_libraries", :action => "index", notice: 'User library was successfully created.'}

        #format.html { redirect_to @user_library, notice: 'User library was successfully created.' }
        #format.json { render json: @user_library, status: :created, location: @user_library }
      else
        format.html { render action: "new" }
        format.json { render json: @user_library.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /user_libraries/1
  # PUT /user_libraries/1.json
  def update
    @user_library = UserLibrary.find(params[:id])

    respond_to do |format|
      if @user_library.update_attributes(params[:user_library])
        format.html { redirect_to user_libraries_url }
        format.json { head :ok }
      
      else
        format.html { render action: "edit" }
        format.json { render json: @user_library.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_libraries/1
  # DELETE /user_libraries/1.json
  def destroy
    @user_library = UserLibrary.find(params[:id])
    @user_library.destroy

    respond_to do |format|
      format.html { redirect_to user_libraries_url }
      format.json { head :ok }
    end
  end


end
