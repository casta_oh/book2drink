class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    #https://github.com/mkdynamic/omniauth-facebook

    @user = User.find_for_facebook_oauth(request.env["omniauth.auth"], current_user)
    puts "El usuario se ha autenticado"
    puts @user



    omniauth_hash = env["omniauth.auth"]
    puts "user.persisted " + @user.persisted?.to_s
    
    if @user.persisted?
      #flash[:notice] = omniauth_hash.info
      sign_in_and_redirect @user, :event => :authentication

    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
end

