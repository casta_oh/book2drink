class AdminController < ApplicationController
  layout 'admin_layout'
  protect_from_forgery
  before_filter :verify_admin

  def console
    
  end

  def users

    @users = User.all
    respond_to do |format|
      format.html # users.html.erb
      format.json { render json: @users }
    end

  end

  def locations

    @locations = Location.all
    @locationMaps = Location.all.to_gmaps4rails
    respond_to do |format|
      format.html # locations.html.erb
      format.json { render json: @locations }
    end

  end

  def isbninfos

    @isbninfos = Isbninfo.all

    respond_to do |format|
      format.html # isbninfos.html.erb
      format.json { render json: @isbninfos }
    end

  end

end
