class CommentlocationsController < ApplicationController
  protect_from_forgery
  before_filter :verify_admin, :only => [:index, :edit, :show, :update]

  # GET /commentlocations
  # GET /commentlocations.json
  def index
    @commentlocations = Commentlocation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @commentlocations }
    end
  end

  # GET /commentlocations/1
  # GET /commentlocations/1.json
  def show
    @commentlocation = Commentlocation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @commentlocation }
    end
  end

  # GET /commentlocations/new
  # GET /commentlocations/new.json
  def new
    #aunque esté esto configurado, nunca pasa por aquí, renderiza directamente form
    @location = Location.find(params[:location_id])
    @commentlocation = @location.commentlocations.build
    @commentlocation.user = current_user
    
  end

  # GET /commentlocations/1/edit
  def edit
    @commentlocation = Commentlocation.find(params[:id])
  end

  # POST /commentlocations
  # POST /commentlocations.json
  def create
    @commentlocation = Commentlocation.new(params[:commentlocation])
    @location = Location.find(@commentlocation.location_id)
    respond_to do |format|
      if @commentlocation.save
        Activity.create_activity(
            current_user.id,
            Book2Drink::Application.config.activitytypecommentlocation,
            @commentlocation.id)
        format.html { redirect_to @location, notice: 'Gracias por comentar el bar.' }
        format.json { render json: @commentlocation, status: :created, location: @commentlocation }
      else
        format.html { render action: "new" }
        format.json { render json: @commentlocation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /commentlocations/1
  # PUT /commentlocations/1.json
  def update
    @commentlocation = Commentlocation.find(params[:id])

    respond_to do |format|
      if @commentlocation.update_attributes(params[:commentlocation])
        format.html { redirect_to @commentlocation, notice: 'Commentlocation was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @commentlocation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /commentlocations/1
  # DELETE /commentlocations/1.json
  def destroy
    @commentlocation = Commentlocation.find(params[:id])
    @commentlocation.destroy

    respond_to do |format|
      format.html { redirect_to commentlocations_url }
      format.json { head :ok }
    end
  end
end
