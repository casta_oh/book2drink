class EventsController < ApplicationController
  protect_from_forgery
  before_filter :verify_admin, :only => [:edit, :update, :new, :create]

  # GET /events
  # GET /events.json
  def index
    if params[:location_id] != nil
      @location = Location.find(params[:location_id])
      @events = @location.events.order('event_date DESC').limit(5)
    else
      @events = Event.order('event_date DESC').limit(5).all
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @events }
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @location = Location.find(params[:location_id])
    @events = @location.events.order('event_date DESC').limit(5)
    @event = @events.find(params[:id])


    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @event }
    end
  end

  # GET /events/new
  # GET /events/new.json
  def new
    @event = Location.find(params[:location_id]).events.build
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @event }
    end
  end

  # GET /events/1/edit
  def edit
    @event = Event.find(params[:id])
  end

  # POST /events
  # POST /events.json
  def create
    
    @event = Event.new(params[:event])
    respond_to do |format|
      if @event.save

        @location = @event.location

        format.html { redirect_to @location, notice: 'Event was successfully created.' }
        format.json { render json: @location, status: :created, location: @location }
      else
        format.html { render action: "new" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /events/1
  # PUT /events/1.json
  def update
    @event = Event.find(params[:id])

    respond_to do |format|
      if @event.update_attributes(params[:event])
        format.html { redirect_to @event.location, notice: 'Event was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event = Event.find(params[:id])
    @event.destroy

    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :ok }
    end
  end
  
  def delete
      @event = Event.find(params[:id])
      @event.destroy
      redirect_to :events
  end
  
end
