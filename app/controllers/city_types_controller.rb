class CityTypesController < ApplicationController
  protect_from_forgery
  before_filter :verify_admin
  
  # GET /city_types
  # GET /city_types.json
  def index
    @city_types = CityType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @city_types }
    end
  end

  # GET /city_types/1
  # GET /city_types/1.json
  def show
    @city_type = CityType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @city_type }
    end
  end

  # GET /city_types/new
  # GET /city_types/new.json
  def new
    @city_type = CityType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @city_type }
    end
  end

  # GET /city_types/1/edit
  def edit
    @city_type = CityType.find(params[:id])
  end

  # POST /city_types
  # POST /city_types.json
  def create
    @city_type = CityType.new(params[:city_type])

    respond_to do |format|
      if @city_type.save
        format.html { redirect_to @city_type, notice: 'City type was successfully created.' }
        format.json { render json: @city_type, status: :created, location: @city_type }
      else
        format.html { render action: "new" }
        format.json { render json: @city_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /city_types/1
  # PUT /city_types/1.json
  def update
    @city_type = CityType.find(params[:id])

    respond_to do |format|
      if @city_type.update_attributes(params[:city_type])
        format.html { redirect_to @city_type, notice: 'City type was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @city_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /city_types/1
  # DELETE /city_types/1.json
  def destroy
    @city_type = CityType.find(params[:id])
    @city_type.destroy

    respond_to do |format|
      format.html { redirect_to city_types_url }
      format.json { head :ok }
    end
  end
end
