class RegistrationController < Devise::RegistrationsController
  protect_from_forgery

  def new
    super
  end

  def create
    # debo meter aquí mi código fuente
    build_resource
    puts "build_resource es"
    puts build_resource
    
    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_in(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with resource
    end


  end

  def update
    @user = User.find(current_user.id)
    puts @user
    puts @user.provider
    if (@user.provider.to_s == "")
      super
    else 
      if @user.update_without_password(params[:user])
        sign_in @user, :bypass => true
        redirect_to root_path
      else
        render "edit"
      end
    end

   
  end

  def step1
    puts "Estoy en el paso 1"
    @user = current_user
    @cities = CityTypes.all

  end



end
