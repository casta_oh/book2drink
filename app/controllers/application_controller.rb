class ApplicationController < ActionController::Base
  protect_from_forgery

  #helper_method :current_user
  helper_method :muestraLogin
  helper_method :listReleasedBooks
  helper_method :listDonatedBooks
  helper_method :lastEvents
  helper_method :last_activities
  helper_method :last_activities_user
  helper_method :get_info_libro
  helper_method :get_comment_location
  helper_method :get_comment_book
  helper_method :lastBooksRegistered
  helper_method :isRegisteredInBook2Drink
  helper_method :is_admin
  helper_method :verify_admin


  private

     def is_admin
       current_user.try(:admin?)
     end

     def verify_admin
        if !is_admin
          redirect_to root_url
        end
     end

     def current_user_session
       return @current_user_session if defined?(@current_user_session)
       @current_user_session = UserSession.find
     end
    
     
     def user_library_paginated
       #@user_libraries = UserLibrary.where(:user_id => current_user.id).paginate(:page => params[:page], :per_page => Book2Drink::Application.config.resultsperpage)
       @user_libraries = UserLibrary.where(:user_id => current_user.id).order('created_at DESC').page(params[:page]).per(5)

       return @user_libraries
     end

     def user_library
       #@user_libraries = UserLibrary.where(:user_id => current_user.id).paginate(:page => params[:page], :per_page => Book2Drink::Application.config.resultsperpage)
       @user_libraries = UserLibrary.where(:user_id => current_user.id).order('created_at DESC')
       
       return @user_libraries
     end

    def user_library_by_id(user_id)
       #@user_libraries = UserLibrary.where(:user_id => current_user.id).paginate(:page => params[:page], :per_page => Book2Drink::Application.config.resultsperpage)
       @user_libraries = UserLibrary.where(:user_id => user_id).order('created_at DESC')
       return @user_libraries
     end


    def muestraLogin
      @user_session = UserSession.new
      render 'user_sessions#new'
      
    end

    def listReleasedBooks
        @listBooks = Isbninfo.find(:all, :limit => 5).reverse
        return @listBooks
    end
    
    def listDonatedBooks
          @listBooks = Isbninfo.find(:all, :limit => 5).reverse
          return @listBooks
    end
    def lastEvents
           @events = Event.find(:all, :order => "event_date desc", :limit => 5).reverse
           return @events
    end
    def last_activities

            #Primero obtengo el usuario actual.
            #a partir del usuario obtengo sus librerías
            @user = current_user
            @userActivities = @user.activities
            @userActivities = @userActivities.order('id DESC').limit(10)
            return @userActivities
    end
    def last_activities_user (user)
            @userActivities = user.activities
            @userActivities = @userActivities.order('created_at DESC').limit(10)
            return @userActivities
    end
    def get_info_libro(isbn_id)
      @book = Isbninfo.find(isbn_id)
    end

    def get_comment_location(comment_id)
      @comment = Commentlocation.find(comment_id)
    end

    def get_comment_book(comment_id)
      @comment = Commentbook.find(comment_id)
    end

    def lastBooksRegistered
           @booksRegistered = Isbninfo.find(:all, :limit => 6, :order => 'created_at DESC')
           return @booksRegistered
    end

    def isRegisteredInBook2Drink(isbn13, isbn10)
          #este método es para saber si un libro está ya registrado en la base de datos o no

    end

    

end
