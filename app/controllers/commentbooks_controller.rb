class CommentbooksController < ApplicationController
  protect_from_forgery
  before_filter :verify_admin, :only => [:index, :edit, :show, :update]

  # GET /commentbooks
  # GET /commentbooks.json
  def index
    
    @isbninfo = Isbninfo.find(params[:isbninfo_id])
    @commentbooks = @isbninfo.commentbooks
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @commentbooks  }
    end
  end

  # GET /commentbooks/1
  # GET /commentbooks/1.json
  def show
    @commentbook = Commentbook.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @commentbook }
    end
  end

  # GET /commentbooks/new
  # GET /commentbooks/new.json
  def new
              #@isbninfo = Isbninfo.find(params[:isbninfo_id])
              #@commentbook = @isbninfo.commentbooks.build
              #@commentbook.user = current_user
  end

  # GET /commentbooks/1/edit
  def edit
    @commentbook = Commentbook.find(params[:id])
  end

  
  #def create
  #    @isbninfo = Isbninfo.find(params[:isbninfo_id])
  #    @comment = @isbninfo.commentbooks.create(params[:comment])
  #    
  #    format.html { redirect_to isbninfo_url(@isbninfo), notice: 'Has creado el comentario con exito.' }
  #end

  # POST /commentbooks
  # POST /commentbooks.json
  def create
    @commentbook = Commentbook.new(params[:commentbook])
    @isbninfo = Isbninfo.find(@commentbook.isbninfo_id)
    respond_to do |format|
      if @commentbook.save
        Activity.create_activity(
          current_user.id,
          Book2Drink::Application.config.activitytypecommentbook,
          @commentbook.id)
        format.html { redirect_to @isbninfo, notice: 'Gracias por comentar el bar.' }
        format.json { render json: @commentbook, status: :created, location: @commentbook }
      else
        format.html { render action: "new" }
        format.json { render json: @commentbook.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /commentbooks/1
  # PUT /commentbooks/1.json
  def update
    @commentbook = Commentbook.find(params[:id])

    respond_to do |format|
      if @commentbook.update_attributes(params[:commentbook])
        format.html { redirect_to @commentbook, notice: 'Commentbook was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @commentbook.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /commentbooks/1
  # DELETE /commentbooks/1.json
  def destroy
    @commentbook = Commentbook.find(params[:id])
    @commentbook.destroy

    respond_to do |format|
      format.html { redirect_to commentbooks_url }
      format.json { head :ok }
    end
  end
end
