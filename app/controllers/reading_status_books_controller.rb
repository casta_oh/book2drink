class ReadingStatusBooksController < ApplicationController
  protect_from_forgery
  before_filter :verify_admin

  # GET /reading_status_books
  # GET /reading_status_books.json
  def index
    @reading_status_books = ReadingStatusBook.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @reading_status_books }
    end
  end

  # GET /reading_status_books/1
  # GET /reading_status_books/1.json
  def show
    @reading_status_book = ReadingStatusBook.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @reading_status_book }
    end
  end

  # GET /reading_status_books/new
  # GET /reading_status_books/new.json
  def new
    @reading_status_book = ReadingStatusBook.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @reading_status_book }
    end
  end

  # GET /reading_status_books/1/edit
  def edit
    @reading_status_book = ReadingStatusBook.find(params[:id])
  end

  # POST /reading_status_books
  # POST /reading_status_books.json
  def create
    @reading_status_book = ReadingStatusBook.new(params[:reading_status_book])

    respond_to do |format|
      if @reading_status_book.save
        format.html { redirect_to @reading_status_book, notice: 'Reading status book was successfully created.' }
        format.json { render json: @reading_status_book, status: :created, location: @reading_status_book }
      else
        format.html { render action: "new" }
        format.json { render json: @reading_status_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /reading_status_books/1
  # PUT /reading_status_books/1.json
  def update
    @reading_status_book = ReadingStatusBook.find(params[:id])

    respond_to do |format|
      if @reading_status_book.update_attributes(params[:reading_status_book])
        format.html { redirect_to @reading_status_book, notice: 'Reading status book was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @reading_status_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reading_status_books/1
  # DELETE /reading_status_books/1.json
  def destroy
    @reading_status_book = ReadingStatusBook.find(params[:id])
    @reading_status_book.destroy

    respond_to do |format|
      format.html { redirect_to reading_status_books_url }
      format.json { head :ok }
    end
  end
end
