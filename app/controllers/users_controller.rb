class UsersController < ApplicationController
  protect_from_forgery
  before_filter :verify_admin, :only => [:new, :edit, :edit_current, :create,
                                         :update_current, :destroy]

  def login
    
    @user = User.new
    respond_to do |format|
      format.html # _login.html.erb
      format.json { render json: @user }
    end
    
  end
  
  # GET /users
  # GET /users.json
  def index
    @users = User.all

    @users = Kaminari.paginate_array(@users)
              .page(params[:page])
              .per(Book2Drink::Application.config.resultsperpage)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users11 }
    end
    
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  def edit
    @user = User.find(params[:id])
    
  end
  
  # GET /users/1/edit
  def edit_current
    @user = current_user
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        format.html { redirect_to root_url, notice: 'Te has registrado correctamente en Book2Drink.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update_current
    puts "Estoy en update_current"
    @user = User.find(current_user.id)
    if @user.update_without_password(params[:user])
      sign_in @user, :bypass => true
      redirect_to root_path


      #format.html { redirect_to @user, notice: 'Profile updated.' }
      #format.json { head :ok }
    else
      respond_to do |format|
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  

  # PUT /users/1
  # PUT /users/1.json
  def update
    puts "Estoy en update"
    @user = User.find(params[:id])
  
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'Profile updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :ok }
    end
  end
end
