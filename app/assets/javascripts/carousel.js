$(function() {

	//	Basic carousel, no options
	$('#foo0').carouFredSel();

	//	Basic carousel + timer
	$('#foo1').carouFredSel({
		auto: {
			pauseOnHover: 'resume',
			onPauseStart: function( percentage, duration ) {
				$(this).trigger( 'configuration', ['width', function( value ) { 
					$('#timer1').stop().animate({
						width: value
					}, {
						duration: duration,
						easing: 'linear'
					});
				}]);
			},
			onPauseEnd: function( percentage, duration ) {
				$('#timer1').stop().width( 0 );
			},
			onPausePause: function( percentage, duration ) {
				$('#timer1').stop();
			}
		}
	});

	//	Scrolled by user interaction
	$('#foo2').carouFredSel({
		prev: '#prev',
		next: '#next',
		pagination: "#pager2",
		auto: false,
                items: 5

	});

	//	Variable number of visible items with variable sizes
	$('#foo3').carouFredSel({
		width: 360,
		height: 'auto',
		prev: '#prev3',
		next: '#next3',
		auto: false
	});

	//	Fluid layout example 1
	$('#fooF0').carouFredSel({
		items: {
			visible: 1,
			width: 'variable'
		}
	});
	$(window).resize(function() {
		var newWidth = $(window).width();
		$('#fooF0').width( newWidth * $('#fooF0').children().length ); // set width of carousel, to ensure the items fit next to eachother
		$('#fooF0').parent().width( newWidth ); // set width of carousel-wrapper
		$('#fooF0').children().width( newWidth - 22 ); // set width of items, -22px for border and margin
	});

	//	Fuild layout example 2
	$(window).resize(function() {
		var newWidth = $(window).width();
		$('#fooF1').carouFredSel({
			width: newWidth,
			scroll: 2
		});
	});
	
	$(window).resize();
});