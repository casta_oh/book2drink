// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

function ManageTabPanelDisplay() {
    var idlist = new Array(
    'divBorderListadoLibrosGoogle',
    'divBorderListadoLibrosBook2Drink');

    // No other customizations are necessary.
    if(arguments.length < 1) { return; }
    for(var i = 0; i < idlist.length; i++) {
      var block = false;
      for(var ii = 0; ii < arguments.length; ii++) {
        if(idlist[i] == arguments[ii]) {
          block = true;
          break;
        }
      }
      if(block) { document.getElementById(idlist[i]).style.display = "block"; }
      else { document.getElementById(idlist[i]).style.display = "none"; }
    }
 }