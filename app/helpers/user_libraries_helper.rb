module UserLibrariesHelper

  #Método que comprueba antes de guardar una user library que la user library ya existe
  def save_userlibrary_check_repetition(p_userlibrary)

    userlibrary = UserLibrary.find_by_isbninfo_id_and_user_id(p_userlibrary.isbninfo_id, p_userlibrary.user_id)
    puts "La libreria es nueva en la base de datos? " + userlibrary.nil?.to_s
    if (userlibrary != nil)
      return userlibrary
    else
      p_userlibrary.save
      Activity.create_activity(
            current_user.id,
            Book2Drink::Application.config.activitytypebookaddedtolibrary,
            p_userlibrary.isbninfo_id)
      return p_userlibrary
    end
  end


end
