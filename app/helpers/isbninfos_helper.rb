module IsbninfosHelper

  #método que comprueba antes de crear un libro que el libro ya existe
  def save_isbninfo_check_repetition(p_isbninfo)
    isbninfo = Isbninfo.find_by_isbn13_and_isbn10(p_isbninfo.isbn13, p_isbninfo.isbn10)
    puts "El libro es nuevo en la base de datos? " + isbninfo.nil?.to_s
    if (isbninfo != nil)
      return isbninfo
    else
      p_isbninfo.save
      return p_isbninfo
    end
  end


end
