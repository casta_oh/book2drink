# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Book2Drink::Application.initialize!

Rails.logger = Logger.new("prueba de log")

Encoding.default_external = Encoding::UTF_8

#necesario para que aws vaya al bucket de irlanda
AWS::S3::DEFAULT_HOST = "s3-eu-west-1.amazonaws.com"
