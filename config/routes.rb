Book2Drink::Application.routes.draw do
  
  devise_for :users, :controllers => { :sessions => "devise/sessions",
                                       :omniauth_callbacks => "users/omniauth_callbacks" ,
                                       :registrations => "registration"
                                     }
  resources :users do
    resources :commentbooks
    resources :user_libraries
  end
  #resources :activities
  #resources :activity_types
  #resources :book_types
  #para restringir los direccionamientos
  #resources :photos, :only => [:index, :show]  
  
  resources :events
  #resources :promotions
  #resources :reading_status_books
  resources :user_libraries do
    #resources :reading_status_books
    collection do
      get 'public_userlibrary/:id', :action => 'public_userlibrary', :as => 'public_userlibrary'
    end
  end
  resources :city_types do
    resources :locations
  end
  resources :commentlocations
  resources :locations do
    resources :commentlocations
    #resources :promotions
    resources :events
  end
  
  resources :commentbooks
  resources :isbninfos do 
    resources :commentbooks
    resources :user_libraries
    collection do
      get 'indexsearchbooks'
      post 'addtobooks'
    end
  end
  match "aftersignin_step1" => "registrati"

  #routes para el administrador
  get "admin/users"
  get "admin/locations"
  get "admin/isbninfos"
  get "admin/console"
  get "main/index"

  match "tour" => "main#tour"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'main#index'




  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
